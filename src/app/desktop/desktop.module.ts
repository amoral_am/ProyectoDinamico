import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DesktopHomeComponent } from './desktop-home/desktop-home.component';
import { ComponentsModule } from '../components/components.module';


export const DESKTOP_ROUTES: Routes = [
  {
    path: '',
    redirectTo:'inicio'
    //component: DesktopHomeComponent,
    // children: [
    //   {
    //     path: 'dynamic-component',
    //     component: PlaceholderComponent
    //   }
    // ]
  },
  {
    path: 'inicio',
    loadChildren: () => import('./pages/inicio/inicio.module').then( m => m.InicioPageModule)
  }
];


@NgModule({
  declarations: [DesktopHomeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(DESKTOP_ROUTES)
  ]
})
export class DesktopModule {
  constructor() {
    console.log('🔥 Loaded DesktopModule');
  }
}
