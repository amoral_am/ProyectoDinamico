import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, Route } from '@angular/router';
import { Platform } from '@ionic/angular';
import { LazyLoaderService } from './lazy-loader.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})


export class AppComponent implements OnInit {
  routes: Route[];
  desktop:boolean=true;

  
  constructor(
    private router: Router,
    private lazyLoaderService: LazyLoaderService,
    public platform: Platform
  ) {}
  

  ngOnInit(): void {
    if ((this.platform.is('desktop') || this.platform.is('tablet')) && !this.isDesktopRouteAvailable()) {
      this.loadDesktopModule()
    }else if(!this.isMovilRouteAvailable()){
      this.loadMovilModule()
    }
    // if(!this.desktop && !this.isMovilRouteAvailable()){
    //   this.loadMovilModule()
    // }else if(this.desktop && !this.isDesktopRouteAvailable()) {
    //   this.loadDesktopModule()
    // }


    // this.router.events.subscribe(async routerEvent => {
    //   if (routerEvent instanceof NavigationStart) {
    //     console.log('routerEvent: ',routerEvent.url)
    //     if (routerEvent.url.includes('lazy') && !this.isLazyRouteAvailable()) {
    //       this.loadLazyModule(routerEvent.url);
    //       this.loadLazyModule()
    //     }
    //     if (routerEvent.url.includes('desktop') && !this.isDesktopRouteAvailable()) {
    //       this.loadLazyModule(routerEvent.url);
    //       this.loadDesktopModule()
    //     }
    //   }
    // });
    this.routes = this.router.config;

    
  }

  loadDesktopModule(url?: string): void {
    this.lazyLoaderService.loadDesktopModules().subscribe(() => {
      const config = this.router.config;
      config.push({
        path: 'desktop',
        loadChildren: () => this.lazyLoaderService.getLazyModule('desktop')
      });
      this.router.resetConfig(config);
      this.router.navigate([url ? url : 'desktop']);
      this.routes = this.router.config;
    });
  }

  loadMovilModule(url?: string): void {
    this.lazyLoaderService.loadMovilModules().subscribe(() => {
      const config = this.router.config;
      config.push({
        path: 'movil',
        loadChildren: () => this.lazyLoaderService.getLazyModule('movil')
      });
      this.router.resetConfig(config);
      this.router.navigate([url ? url : 'movil']);
      this.routes = this.router.config;
    });
  }


  private isDesktopRouteAvailable(): boolean {
    return this.router.config.filter(c => c.path === 'desktop').length > 0;
  }

  private isMovilRouteAvailable(): boolean {
    return this.router.config.filter(c => c.path === 'movil').length > 0;
  }
}
