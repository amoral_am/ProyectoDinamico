import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponenteCompartidoComponent } from './componente-compartido/componente-compartido.component';
import { ComponenteDinamicoComponent } from './componente-dinamico/componente-dinamico.component';



@NgModule({
  declarations: [
    ComponenteCompartidoComponent,
    ComponenteDinamicoComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ComponenteCompartidoComponent,
    ComponenteDinamicoComponent
  ]
})
export class ComponentsModule { }
