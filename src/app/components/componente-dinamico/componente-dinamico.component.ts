import { CommonModule } from '@angular/common';
import { Compiler, Component, NgModule, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-componente-dinamico',
  template: '<div #container></div>',
})
export class ComponenteDinamicoComponent implements OnInit {
  @ViewChild('container', { read: ViewContainerRef, static: false }) container: ViewContainerRef;
  plantilla
  estilo

  constructor(
    private compiler: Compiler,
    public platform: Platform
  ) { }

  ngOnInit() {}

  ngAfterViewInit() {
    console.log('Plataforma',this.platform.platforms())

    if (this.platform.is('desktop') || this.platform.is('tablet')) {
      this.plantilla = require('./views/WebView/ComponenteDinamicoWeb.html')
      this.estilo = require('./views/WebView/ComponenteDinamicoWeb.scss')
      // Must clear cache.
      this.compiler.clearCache();
      // Define the component using Component decorator.
      const component = Component({
        template: this.plantilla.default as string,
        //IMPORTANTE ANGULAR12: /*styles: [this.estilo],*/
        styles: [this.estilo.default],
      })(ComponeteDinamicoClass);
      // Define the module using NgModule decorator.
      const ngmodule = NgModule({
        declarations: [component],
        imports: [
          CommonModule,
          //AppMaterialModule
        ]
      })(class {
      });

      // Asynchronously (recommended) compile the module and the component.
      this.compiler.compileModuleAndAllComponentsAsync(ngmodule)
        .then(factories => {
          // Get the component factory.
          const componentFactory = factories.componentFactories[0];
          // Create the component and add to the view.
          const componentRef = this.container.createComponent(componentFactory);
          // let element: HTMLElement = <HTMLElement>componentRef.location.nativeElement;
          // element.style.height = "100%";        
          // element.style.width= "100%";
          //componentRef.instance.variableInput = 'Algo'
        });


    } else {
      this.plantilla = require('./views/MovilView/ComponenteDinamicoMovil.html')
      this.estilo = require('./views/MovilView/ComponenteDinamicoMovil.scss')
      // Must clear cache.
      this.compiler.clearCache();
      // Define the component using Component decorator.
      const component = Component({
        template: this.plantilla.default as string,
        //IMPORTANTE ANGULAR12: /*styles: [this.estilo],*/
        styles: [this.estilo.default],
      })(ComponeteDinamicoClass);
      // Define the module using NgModule decorator.
      const ngmodule = NgModule({
        declarations: [component],
        imports: [
          CommonModule
        ]
      })(class {
      });

      // Asynchronously (recommended) compile the module and the component.
      this.compiler.compileModuleAndAllComponentsAsync(ngmodule)
        .then(factories => {
          // Get the component factory.
          const componentFactory = factories.componentFactories[0];
          // Create the component and add to the view.
          const componentRef = this.container.createComponent(componentFactory);
          // let element: HTMLElement = <HTMLElement>componentRef.location.nativeElement;
          // element.style.height = "100%";        
          // element.style.width= "100%";
          //componentRef.instance.variableInput = 'Algo'
        });

    }


  }


}

export class ComponeteDinamicoClass {
  
}
