import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MovilHomeComponent } from './movil-home/movil-home.component';
import { ComponentsModule } from '../components/components.module';


export const MOVIL_ROUTES: Routes = [
  {
    path: '',
    redirectTo:'inicio'
    //component: MovilHomeComponent,
    // children: [
    //   {
    //     path: 'dynamic-component',
    //     component: PlaceholderComponent
    //   }
    // ]
  },
  {
    path: 'inicio',
    loadChildren: () => import('./pages/inicio/inicio.module').then( m => m.InicioPageModule)
  }
];


@NgModule({
  declarations: [MovilHomeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(MOVIL_ROUTES)
  ]
})

export class MovilModule {
  constructor() {
    console.log('🔥 Loaded MovilModule');
  }
}
