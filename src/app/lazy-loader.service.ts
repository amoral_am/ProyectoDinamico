import { Injectable } from '@angular/core';
import { delay, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LazyLoaderService {
  private lazyMap: Map<string, Promise<unknown>> = new Map();

  constructor() {}

  getLazyModule(key: string): Promise<unknown> {
    return this.lazyMap.get(key);
  }

  loadDesktopModules(): Observable<number | void> {
    return of(1).pipe(
      tap(() => {
        this.lazyMap.set(
          'desktop',
          import('./desktop/desktop.module').then(m => m.DesktopModule)
        );
      })
    );
  }

  loadMovilModules(): Observable<number | void> {
    return of(1).pipe(
      tap(() => {
        this.lazyMap.set(
          'movil',
          import('./movil/movil.module').then(m => m.MovilModule)
        );
      })
    );
  }
}