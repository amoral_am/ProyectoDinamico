# Proyecto Dinámico
Este es un proyecto el cual aprovecha la tecnología Lazy Load de Angular para tener varios módulos de aplicación y cargarlos según nos convenga. En este caso particular, tendremos un módulo para la aplicación de escritorio y otro módulo para la aplicacción movil. Este proyecto adapta el siguiente tutorial: https://mokkappsdev.medium.com/manually-lazy-load-modules-and-components-in-angular-99b845104ef9 al caso particular descrito anteriormente.

## Plantillas Dinámicas
Además de utilizar distintos modos en función de en que plataforma utilicemos la APP, tendremos componentes con diferente aspecto en función de la plataforma en la que lo visualicemos.

### Configuración (No es necesaria)
A continuación, se muestran los cambios que se han hecho a un proyecto de IONIC normal para que este proyecto funcione correctamente. (Estos pasos no son necesarios hacerlos dentro de este proyecto, ya que en el proyecto subido se encuentran realizados. Es simplemente para saber como se ha hecho este proyecto inicial).

- Installar webpack (https://www.youtube.com/watch?v=XijU_6UBOlA).

```sh
npm install @angular-builders/custom-webpack
npm install  @angular-builders/dev-server
```

- Crear el fichero webpack.custom.config.js en la raiz del proyecto, y añadimos:
 ```sh
module.exports = {
    module: {
      rules: [
        {
          test: /\.html$/,
          loader: "raw-loader"
        },
      ]
    }
  };
```

- Modificar el fichero angular.json

```sh
"architect": { 
        "build": { 
          "builder": "@angular-builders/custom-webpack:browser", 
          "options": { 
            "customWebpackConfig":{ 
              "path":"./webpack.custom.config.js" 
            }, 
..... 
"serve": { 
          "builder": "@angular-builders/custom-webpack:dev-server", 
.... 
```
- Instalar text-loader. 
```sh
npm install --save-dev  text-loader
```
- Instalar raw-loader. 
```sh
npm install --save-dev  raw-loader
```
- Instalar type/node.
```sh
npm i @types/node --save-dev
```
- Añadir type node a fichero tsconfig.app.json.
```sh
"types": [ 
      "node" 
    ]  
```
### IMPORTANTE
En Angular 12, cuando cargamos los estilos en el componente dinámico:
```
//IMPORTANTE ANGULAR12: 
styles: [this.estilo],
//IMPORTANTE < ANGULAR12: 
styles: [this.estilo.default],
```

## Ejecutar el proyecto
Una vez descargado el proyecto. Ejecutar:
```sh
npm install
```

```sh
ionic serve
```


## Modificaciones para convertir aplicación en PWA con webpack
Puede que este error no ocurra siempre, pero en caso de ontener el siguiente error:
```sh
Unhandled Promise rejection: Angular JIT compilation failed: '@angular/compiler' not loaded!
  - JIT compilation is discouraged for production use-cases! Consider AOT mode instead.
  ...
  ...
  ...
```
Se puede solucionar con los siguientes pasos:
- En fichero main.ts añadir:
```sh
import '@angular/compiler'
```
- Actualizar todas las librerías
- En fichero angular.json:
```sh
...
"aot": false
...
"buildOptimizer": false
```